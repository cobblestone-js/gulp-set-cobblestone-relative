var through = require("through2");
var pumpify = require("pumpify");
var merge = require("merge");
var dotted = require("dotted");
var path = require("path");
var zpad = require("zpad");

module.exports = function(params)
{
    // Parse and figure out all the parameters.
    if (!params) params = {};
    params.property = params.property || "page.relative";
    params.pathRegex = params.pathRegex || /-(\d+?)\/$/;

    // Figure out the next numerical index. The input will be a zero-padded
    // number that we have to match in padding but get the before and after
    // values. If we have an invalid value, we use the didNotMatchPattern to
    // figure out we don't have it.
    var didNotMatchPattern = "//DID NOT MATCH//";

    function getZeroPaddedOffset(found, index, offset)
    {
        // Convert the index to a number and add the offset.
        var newIndex = parseInt(index) + offset;

        // We don't handle negative values at all.
        if (newIndex < 0)
        {
            return didNotMatchPattern;
        }

        // For everything else, we want to zero-pad it.
        return found.replace(index, zpad(newIndex, index.length));
    }

    function getOffsetPath(file, label, offset)
    {
        // First see if the user or another program specifically provided a path.
        var search = params.property + "." + label + "Path";
        var path = dotted.getNested(file, search);

        // If we don't have a path, then try to figure it out from the name.
        if (!path)
        {
            var match = file.data.relativePath.replace(
                params.pathRegex,
                function (found, index) {
                    return getZeroPaddedOffset(found, index, offset);
                });
            var isInvalid = match.indexOf(didNotMatchPattern) >= 0;
            var isIdentical = match === file.data.relativePath;

            path = isInvalid || isIdentical ? null : match;
        }

        // Return the resulting path, which will be a valid or null.
        return path;
    }

    // Set up the scanner as an inner pipe that goes through the files and
    // loads the metadata into memory.
    var files = {};

    var scanPipe = through.obj(
        function(file, encoding, callback)
        {
            // Copy the file and remove the contents to avoid nesting endless
            // files inside themselves.
            var data = merge({}, file.data);
            delete data.contents;
            files[data.relativePath] = data;

            // Return the file to let the pipe run through.
            return callback(null, file);
        });

    // We have a second pipe that does the actual manipulation to the files
    // before emitting.
    var updatePipe = through.obj(
        {
            // We need a highWaterMark larger than the totalf iles being processed
            // to ensure everything is read into memory first before writing it out.
            // There is no way to disable the buffer entirely, so we just give it
            // the highest integer value.
            highWaterMark: 2147483647
        },
        function(file, encoding, callback)
        {
            // Figure out the relative paths to the before and after values.
            // This will be either a full relative path or a falsy value.
            var beforePath = getOffsetPath(file, "before", -1);
            var afterPath = getOffsetPath(file, "after", 1);

            // Look up the values in our hash.
            var before = !beforePath ? null : files[beforePath];
            var after = !afterPath ? null : files[afterPath];

            // If we have a valid, then set it using the given path.
            if (before)
            {
                dotted.setNested(
                    file,
                    params.property + ".before",
                    before,
                    { ensure: true });
            }

            if (after)
            {
                dotted.setNested(
                    file,
                    params.property + ".after",
                    after,
                    { ensure: true });
            }

            // We modified the file, so mark it done.
            return callback(null, file);
        });

    // We have to cork() updatePipe. What this does is prevent updatePipe
    // from getting any data until it is uncork()ed, which we won't do, or
    // the scanPipe gets to the end.
    updatePipe.cork();

    // We have to combine all of these pipes into a single one because
    // gulp needs a single pipe  but we have to treat these all as a unit.
    return pumpify.obj(scanPipe, updatePipe);
}
